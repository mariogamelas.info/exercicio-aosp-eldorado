/* Primeira aplicacao nativa em C para Android */ 
#include <stdio.h>
#include "android/log.h"

int main(void)
{
    __android_log_write(ANDROID_LOG_INFO, "HELLO_WORLD", "Iniciando aplicação");
    printf("\r\nHello world\n");
    printf("\r\nEsta eh minha primeira aplicacao em C nativa para Android!!!\n");
    return 0;
}
