package com.eldorado.aospbase;

import android.app.Activity;
import android.widget.TextView;
import android.os.Bundle;

import android.content.pm.ApplicationInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;


public class AospBase extends Activity
{
    private LinearLayout mLayoutBase;
    private final String[] mNomesEquipe = {
            "Equipe",
            "",
            "Alexandre de Barros",
            "Diego Ramon Costa Machado",
            "João Vicente Silva Goes",
            "Thales Ruano Barros de Souza",
            "Thiago Trindade",
            "Mário Paixão Gamelas"
    };
    private int mContadorPressionado = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        layoutInicial();
    }

    private void layoutInicial()
    {
        LinearLayout.LayoutParams botaoParam =
                new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);

        Button botaoInicio = new Button(this);
        botaoInicio.setText("Pressione");
        botaoInicio.setLayoutParams(botaoParam);
        botaoInicio.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onClickPressione();
            }
        });

        mLayoutBase = new LinearLayout(this);
        mLayoutBase.setOrientation(LinearLayout.VERTICAL);
        mLayoutBase.setHorizontalGravity(Gravity.CENTER);
        mLayoutBase.setVerticalGravity(Gravity.TOP);
        mLayoutBase.addView(botaoInicio);

        setContentView(mLayoutBase);
    }

    private void onClickPressione()
    {
        LinearLayout.LayoutParams textoParam =
                new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);

        if (mContadorPressionado < mNomesEquipe.length)
        {
            TextView novoTexto = new TextView(this);
            novoTexto.setLayoutParams(textoParam);
            novoTexto.setText(mNomesEquipe[mContadorPressionado]);
            mLayoutBase.addView(novoTexto);
        }else if (mContadorPressionado == mNomesEquipe.length)
        {
            TextView novoTexto = new TextView(this);
            novoTexto.setLayoutParams(textoParam);
            String msgDebug = "Aplicativo do sistema: " + checaAppSistema();
            novoTexto.setText(msgDebug);
            mLayoutBase.addView(novoTexto);
        }

        mContadorPressionado++;
    }

    private boolean checaAppSistema()
    {
        int todasFlags = this.getApplicationInfo().flags;
        int flagSistema = ApplicationInfo.FLAG_SYSTEM;

        return (todasFlags & flagSistema) != 0;
    }
}
