$(call inherit-product, $(SRC_TARGET_DIR)/product/aosp_x86_64.mk)

PRODUCT_NAME := mario
PRODUCT_DEVICE := mario
PRODUCT_BRAND := Android
PRODUCT_MODEL  := AOSP Mariodroid Emulator

#PRODUCT_PACKAGES += \
#LiveWallpapers	\
#Gallery2	\
#SoundRecorder	\
#Camera	\
#Email	\
#FSLOta	\
#CactusPlayer	\
#VideoEditor	\
#FSLProfileApp \
#FSLProfileService \
#PinyinIME

PRODUCT_COPY_FILES += \
device/eldorado_tech/mario/init.rc:root/init.ranchu.rc	\
device/eldorado_tech/mario/vold.fstab:system/etc/vold.fstab	\
$(LOCAL_PATH)/gpsreset.sh:$(TARGET_COPY_OUT_SYSTEM)/etc/gpsreset.sh
